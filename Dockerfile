FROM golang:1.15-alpine AS build
WORKDIR /build  
COPY . .
RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -ldflags "-s -w -extldflags '-static'" -o ./app

FROM scratch
COPY --from=build /build/app /app
ENTRYPOINT ["/app"]


  
