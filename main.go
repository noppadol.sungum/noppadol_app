package main
import (
    "fmt"
    "net/http"
    "encoding/json" 
    // "bytes"
    // "io/ioutil"
    // "strings" 
)

func main() {

    fmt.Println("enter tom testgolang test np")
    http.HandleFunc("/returnTest", returnTest)   
    http.ListenAndServe(":10094", nil)  
}

type Testtom struct { 
    Title string `json:"Title"`
    Desc string `json:"desc"`
    Content string `json:"content"`
}

var Testtoms []Testtom

func returnTest(w http.ResponseWriter, r *http.Request){
    Testtoms = []Testtom{
        Testtom{Title: "Test1", Desc: "Test1 Description", Content: "Test1 Content test"},
        Testtom{Title: "Test2", Desc: "Test2 Description", Content: "Test2 Content test"},
    }

    fmt.Println("Endpoint Hit: returnTest") 
    json.NewEncoder(w).Encode(Testtoms) 
}

